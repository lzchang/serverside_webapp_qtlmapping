import com.google.code.externalsorting.ExternalSort;
import org.apache.commons.lang.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.stream.Stream;

@javax.servlet.annotation.WebServlet(name = "serverSideProcessing", urlPatterns = "/serverSideProcessing")
public class serverSideProcessing extends javax.servlet.http.HttpServlet {
    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response) throws javax.servlet.ServletException, IOException {
        // Retrieve parameters from request
        Integer start = Integer.parseInt(request.getParameter("start"));
        Integer length = Integer.parseInt(request.getParameter("length"));
        Integer order = Integer.parseInt(request.getParameter("order[0][column]"));
        String direction = request.getParameter("order[0][dir]");
        String search = request.getParameter("search[value]");
        String dataset = request.getParameter("dataset");
        String session_id = request.getParameter("_");


        // Read input file and obtain the headers and data(rows)
        ArrayList<ArrayList> returned_data = readfile(System.getProperty("500fg.data.dir") + dataset, search, order, direction, start, length, session_id);
        ArrayList<String> headers = returned_data.get(0);
        ArrayList<String> rows = returned_data.get(1);
        int total = (int) returned_data.get(2).get(0);
        int totalfiltered = (int) returned_data.get(2).get(1);

        if (rows.isEmpty()) {
            CreateJsonResponse(new JSONArray(), totalfiltered, total, request.getParameter("draw"), response);

        } else {
            JSONArray returned_entries = new JSONArray();

            for (String test : rows) {
                String[] splited = test.split("\t");

                JSONObject cell = new JSONObject();
                for (int y = 0; y < splited.length; y++) {
                    cell.put(headers.get(y), splited[y]);
                }
                returned_entries.put(cell);

            }

            CreateJsonResponse(returned_entries, totalfiltered, total, request.getParameter("draw"), response);
        }
    }

    private void CreateJsonResponse(JSONArray data, int n_filtered, int n_total, String draw, HttpServletResponse response) throws IOException {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("data", data);
        jsonObject.put("recordsFiltered", n_filtered);
        jsonObject.put("recordsTotal", n_total);
        jsonObject.put("draw", draw);

        response.setContentType("application/Json");
        response.getWriter().print(jsonObject);
    }

    private ArrayList<ArrayList> readfile(String dataset, String search, Integer order, String direction, Integer start, Integer length, String session_id) throws IOException {
        Comparator<String> newComparator = new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                String split1 = o1.split("\t")[order];
                String split2 = o2.split("\t")[order];
                if (NumberUtils.isNumber(split1)) {
                    if ((Double.parseDouble(split1) % 1) == 0) {
                        Integer splittedNumber = Integer.parseInt(split1);
                        return splittedNumber.compareTo(Integer.parseInt(split2));
                    } else {
                        Double splittedNumber = Double.parseDouble(split1);
                        return splittedNumber.compareTo(Double.parseDouble(split2));
                    }
                } else {
                    return split1.compareTo(split2);
                }
            }

        };

        // Arraylists for storing data
        ArrayList<String> dataList = new ArrayList<String>(length);
        ArrayList<String> headers = new ArrayList<String>();

        // Get header from file.
        try (Stream<String> lines = Files.lines(Paths.get(dataset))) {
            headers.addAll(Arrays.asList(lines.skip(0).findFirst().get().split("\t")));
        }

        String orderedDataset = dataset.substring(0, dataset.length() - 4) + "_" + order + "_" + direction + ".txt";

        // If sorted file doesnt exist, make new file
        if (!Files.exists(Paths.get(dataset.substring(0, dataset.length() - 4) + "_" + order + "_" + direction + ".txt"))) {
            ExternalSort.mergeSortedFiles(ExternalSort.sortInBatch(new File(dataset), newComparator, Charset.defaultCharset(), null, false, 1), new File(orderedDataset));
        }

        // Count amount of lines in file
        FileInputStream stream = new FileInputStream(orderedDataset);
        byte[] buffer = new byte[8192];
        int n_lines = 0;
        int n;
        while ((n = stream.read(buffer)) > 0) {
            for (int i = 0; i < n; i++) {
                if (buffer[i] == '\n') n_lines++;
            }
        }
        stream.close();

        int posStart = start;
        int posEnd = start + length;

        // Get the specific lines in file to read

        // Read through sorted file and only save the required n lines, n=length
        BufferedReader buffReader = new BufferedReader(new FileReader(orderedDataset));
        int index = 0;
        String line;

        while ((line = buffReader.readLine()) != null) {
            if (line.toLowerCase().contains(search.toLowerCase())) {
                if (direction.equals("asc")) {
                    if (index >= posStart && index < posEnd) {
                        dataList.add(line);
                    }
                }
                index++;
            }
        }

        buffReader.close();

        if (direction.equals("desc")) {
            posStart = index - start;
            posEnd = index - (start + length);

            buffReader = new BufferedReader(new FileReader(orderedDataset));
            index = 0;

            while ((line = buffReader.readLine()) != null) {
                if (line.toLowerCase().contains(search.toLowerCase())) {
                    if (direction.equals("desc")) {
                        if (index < posStart && index >= posEnd) {
                            dataList.add(line);
                        }
                    }
                    index++;
                }
            }
            buffReader.close();

            Collections.reverse(dataList);

        }

        // Combine all obtained information into a single arraylist
        ArrayList<Integer> entries = new ArrayList<>(2);
        entries.add(n_lines);
        entries.add(index);

        ArrayList<ArrayList> combinedData = new ArrayList<ArrayList>(3);
        combinedData.add(headers);
        combinedData.add(dataList);
        combinedData.add(entries);

        System.out.println("done reading");
        return combinedData;
    }
}