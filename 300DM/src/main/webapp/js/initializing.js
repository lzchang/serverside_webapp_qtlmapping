function clearPage() {
    // Clear the page to make space for new content
    $("#text_area").empty();
    $("#table_area").empty();
    $("#set_selection").empty();
    $(".dropdown").removeClass("active");
}

function fillTable(datasetToOpen, cols) {
    var fullarray = [];
    for (x = 0; x < cols.length; x++) {
        fullarray[x] = {"data": cols[x]};

        // First row should be the header which gets added to the head of the created table
        var tr = document.getElementById('table_id').tHead.children[0],
            th = document.createElement('th');

        th.innerHTML = cols[x];
        tr.appendChild(th);

    }

    // Initialize dataTable

    mytable = $('#table_id').DataTable({
        "searchDelay": 3000,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "serverSideProcessing",
            "data": {dataset: datasetToOpen}
        },
        "columns": fullarray,
        dom: "fBltipr",
        language: {
            searchPlaceholder: "",
            search: "Search: "
        },
        buttons: [
            'copy', 'csv']
    });
}

$(document).ready(function () {
    $(".cQTL, .ccQTL").click(function () {
        var text;
        var header = "<h2>" + $(this).text().split(" ")[0] + "</h2>";

        // Define the dataset to open and the text to show on the page.
        if ($(this).attr('class').split(" ")[1] === "cQTL") {
            datasetToOpen = "300DM_cQTL_multivar.txt";
            text = header + "Multivariate QTL mapping was performed\n" +
                "            on cytokine measurements upon stimulation. The selected traits used were based on the cytokine+stimulation\n" +
                "            type, and the used stimulation+time of measurement.<br>Only QTLs with a p-value < 1-e³ are\n" +
                "            shown. The traits included in the model can be accessed <a href='https://docs.google.com/spreadsheets/d/e/2PACX-1vTGvTaLXTdzI7CiWfgBJf_iDlufWrJVlANLnfUV4FiB_RYbJvFK9q7ixftm3leVAZrLVPkuY6ZFErL6/pubhtml'>here</a>.";
            dataColums = ["SNP", "Phenotype", "CHR", "POS", "allele0", "allele1", "af", 'p_wald']
        } else if ($(this).attr('class').split(" ")[1] === "ccQTL") {
            datasetToOpen = "summary_complete_E.Coli24h_1e-3.txt";
            text = header + "This contains the results of the single-trait QTL mapping on PBMC, consisting of lymphocytes which are T cells, B cells and NK-cell, and monocytes. The blood cell counts were measured in baseline.";
        }

        clearPage();
        $("#" + $(this).attr('class').split(" ")[1]).parent().parent().addClass("active");

        // Create an empty table
        $("#text_area").append("<p>" + text + "</p>\n");
        $("#table_area").append("<table id=\'table_id\' class=\'display\'><thead><tr></tr></thead><tbody></tbody></table>");

        if ($(this).attr('class').split(" ")[1] === "mQTL") {
            $("#general").css("font-weight", "bold");
            $("a[id='general']").contents().unwrap();
        }

        fillTable(datasetToOpen, dataColums);

    });
});

