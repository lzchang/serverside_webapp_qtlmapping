function clearPage() {
    // Clear the page to make space for new content
    $("#text_area").empty();
    $("#table_area").empty();
    $("#set_selection").empty();
    $(".dropdown").removeClass("active");
}

function fillTable(datasetToOpen, cols) {
    var fullarray = [];
    for (x = 0; x < cols.length; x++) {
        fullarray[x] = {"data": cols[x]};

        // First row should be the header which gets added to the head of the created table
        var tr = document.getElementById('table_id').tHead.children[0],
            th = document.createElement('th');

        th.innerHTML = cols[x];
        tr.appendChild(th);
    }

    // Initialize dataTable

    mytable = $('#table_id').DataTable({
        "searchDelay": 3000,
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": "serverSideProcessing",
            "data": {dataset: datasetToOpen}
        },
        "columns": fullarray,
        dom: "fBltipr",
        language: {
            searchPlaceholder: "",
            search: "Search: "
        },
        buttons: [
            'copy', 'csv']
    });
}

$(document).ready(function () {
    $(".mQTL, .cQTL500FG").click(function () {
        var text;
        var header = "<h2>" + $(this).text().split(" ")[0] + "</h2>";

        // TODO change the paths to files
        // Define the dataset to open and the text to show on the page.
        if ($(this).attr('class').split(" ")[1] === "cQTL500FG") {
            datasetToOpen = "merged_multi_trait_cqtls_nominal_1e-2.txt";
            text = header + "The human cytokine response shows a remarkable inter-individual heterogeneity in the general population. Previous studies have identified that genetic and non-genetic host factors play an important role in driving such variation. However, these studies focused mostly on single genotype-phenotype pairs and did not take the correlation structure and potential co-regulation between cytokine response phenotypes into consideration. In this study, we aimed to identify the genetic effects that are shared between cytokine response profiles across a wide range of stimuli. Using a multivariate QTL mapping approach, we were able to identify four previously unidentified loci regulating the ex-vivo cytokine response to pathogens in a population of 500 healthy individuals, highlighting the need for more complex models when studying immune responses. This study is part of the Human Functional Genomics Project (HGFP, http://www.humanfunctionalgenomics.org). <br>The results shown are all multi-trait cytokine QTLs at a p<1e-2. ";
            dataColums = ["model", "chr", "rs", "ps", "n_miss", "allele1", "allele0", "af", "beta", "p_wald"]

        } else {
            datasetToOpen = "qtl_out_general_anno_1e-4.txt";
            text = header + "This result is a part of Human Functional Genomics Project (HFGP, http://www.humanfunctionalgenomics.org). We measured 10,434 metabolite features by three different metabolome platforms: NMR (Brain-shake metabolomics, Finland), flow-injection TOF-M (General Metabolomics, Boston) and an integrated measurement system of NMR, gas chromatography-mass spectrometry (GC-MS) and liquid chromatography-mass spectrometry (LC-MS) (untargeted metabolomics, U.S.A), and performed QTL mapping on those metabolite features.<br> SNPs with a p value lower than 1e-4 were presented in this website.<br><br>\n" +
                "\n" +
                "            Please cite the following publication:<br>\n" +
                "            Integration of metabolomics, genomics and immune phenotypes reveals the causal roles of metabolites in disease X.Chu et.al\n" +
                "            </a> <br/><br/><a id='general' href='#'>General metabolics</a> | <a id ='untargeted' href='#'>Untargeted metabolics</a> | <a id='brainshake' href='#'>Brain-shake metabolics</a>";
            dataColums = ["SNP", "m/z", "p-value", "beta", "EA", "AA", "chr", "pos", "Top_annotation_name", "Top_annotation_formula", "Top_annotation_score"]

        }

        clearPage();
        $("#" + $(this).attr('class').split(" ")[1]).parent().parent().addClass("active");

        // Create an empty table
        $("#text_area").append("<p>" + text + "</p>\n");
        $("#table_area").append("<table id=\'table_id\' class=\'display\'><thead><tr></tr></thead><tbody></tbody></table>");

        if ($(this).attr('class').split(" ")[1] === "mQTL") {
            $("#general").css("font-weight", "bold");
            $("a[id='general']").contents().unwrap();
        }

        fillTable(datasetToOpen, dataColums);

    });

    $("#text_area").on('click', 'a', function () {
        var header = "<h2> mQTL </h2>";
        var text = header + "This result is a part of Human Functional Genomics Project (HFGP, http://www.humanfunctionalgenomics.org). We measured 10,434 metabolite features by three different metabolome platforms: NMR (Brain-shake metabolomics, Finland), flow-injection TOF-M (General Metabolomics, Boston) and an integrated measurement system of NMR, gas chromatography-mass spectrometry (GC-MS) and liquid chromatography-mass spectrometry (LC-MS) (untargeted metabolomics, U.S.A), and performed QTL mapping on those metabolite features.<br> SNPs with a p value lower than 1e-4 were presented in this website.<br><br>\n" +
            "\n" +
            "            Please cite the following publication:<br>\n" +
            "            Integration of metabolomics, genomics and immune phenotypes reveals the causal roles of metabolites in disease X.Chu et.al\n" +
            "            </a> <br/><br/><a id='general' href='#'>General metabolics</a> | <a id ='untargeted' href='#'>Untargeted metabolics</a> | <a id='brainshake' href='#'>Brain-shake metabolics</a>";

        if ($(this).attr('id') === "untargeted") {
            datasetToOpen = "qtl_out_untarget_anno_1e-4.txt";
            dataColums = ["SNP", "m/z", "p-value", "beta", "EA", "AA", "chr", "pos", "annotation_name"]

        } else if ($(this).attr('id') === "brainshake") {
            datasetToOpen = "qtl_out_brain-shake_anno_1e-4_corrected.txt";
            dataColums = ["SNP", "id", "p-value", "beta", "EA", "AA", "chr", "pos", "annotation_name"]
        } else {
            datasetToOpen = "qtl_out_general_anno_1e-4.txt";
            dataColums = ["SNP", "m/z", "p-value", "beta", "EA", "AA", "chr", "pos", "Top_annotation_name", "Top_annotation_formula", "Top_annotation_score"]
        }

        clearPage();

        // Create an empty table
        $("#text_area").append("<p>" + text + "</p>\n");
        $("#table_area").append("<table id=\"table_id\" class=\"display\"><thead><tr></tr></thead><tbody></tbody></table>");

        // Make selected option bold
        $("a[id='" + $(this).attr('id') + "']").contents().unwrap();
        $("#" + $(this).attr('id')).css("font-weight", "bold");

        $("#navbarDropdownMenuLink300DM").parent().addClass("active");

        // Fill table
        fillTable(datasetToOpen, dataColums);
    });
});

