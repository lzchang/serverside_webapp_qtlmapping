#QTL mapping browser

This web application displays the results obtained from QTL analysis.  
The results can be divided into the 500FG- and 300DM- cohort. cQTL and mQTL results are available for the 500FG, while the ccQTL and cQTL results are available for the 300DM dataset.


##Installation
* Download from source
* Run index.html within Apache Tomcat (or comparable)

Alternatively the web application can be accessed online at https://500dm-hfgp.bbmri.nl and https://300dm-hfgp.bbmri.nl.
## Required software
* Apache Tomcat or comparable web container
 